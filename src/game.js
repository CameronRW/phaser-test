import Phaser from 'phaser';

import backgroundImage from './assets/background.png';
import playerImage from './assets/player.png';
import enemyImage from './assets/dragon.png';
import treasureImage from './assets/treasure.png';

// create new scene
const gameScene = new Phaser.Scene('Game');

// initial scene parameeter
gameScene.init = function init() {
  // player speed
  this.playerSpeed = 3;

  // enemy speed
  this.enemyMinSpeed = 2;
  this.enemyMaxSpeed = 4.5;

  // boundaries
  this.enemyMinY = 80;
  this.enemyMaxY = 280;

  this.isTerminating = false;
};

// load assets
gameScene.preload = function preload() {
  // load images
  this.load.image('background', backgroundImage);
  this.load.image('player', playerImage);
  this.load.image('enemy', enemyImage);
  this.load.image('treasure', treasureImage);
};

// called once after preload ends
gameScene.create = function create() {
  // create bg sprite
  const bg = this.add.sprite(0, 0, 'background');
  // change the origin to the top left corner
  bg.setOrigin(0, 0);

  // create player
  this.player = this.add.sprite(40, this.sys.game.config.height / 2, 'player');
  // we are reducing the width and height by 50%
  this.player.setScale(0.5);

  // goal
  this.goal = this.add.sprite(this.sys.game.config.width - 80, this.sys.game.config.height / 2, 'treasure');
  this.goal.setScale(0.6);

  // enemy group
  this.enemies = this.add.group({
    key: 'enemy',
    repeat: 5,
    setXY: {
      x: 90,
      y: 100,
      stepX: 80,
      stepY: 20,
    },
  });

  // setting scale to all group elements
  Phaser.Actions.ScaleXY(this.enemies.getChildren(), -0.4, -0.4);

  // set flipX and speed
  Phaser.Actions.Call(this.enemies.getChildren(), (enemy) => {
    const flipper = enemy;

    // flip enemy
    flipper.flipX = true;

    // set speed
    const dir = Math.random() < 0.5 ? 1 : -1;
    const speed = this.enemyMinSpeed + Math.random() * (this.enemyMaxSpeed - this.enemyMinSpeed);
    flipper.speed = dir * speed;
  }, this);
};

// this is called up to 60 times per second
gameScene.update = function update() {
  // don't execute if we are terminating
  if (this.isTerminating) return;

  // check for active input
  if (this.input.activePointer.isDown) {
    // player walks
    this.player.x += this.playerSpeed;
  }

  // treasure overlap check
  const playerRect = this.player.getBounds();
  const treasureRect = this.goal.getBounds();

  // restart the scene
  if (Phaser.Geom.Intersects.RectangleToRectangle(playerRect, treasureRect)) {
    return this.gameOver();
  }

  // get enemies
  const enemies = this.enemies.getChildren();

  enemies.forEach((enemy) => {
    const mover = enemy;

    // enemy movement
    mover.y += enemy.speed;

    const conditionUp = enemy.speed < 0 && enemy.y <= this.enemyMinY;
    const conditionDown = enemy.speed > 0 && enemy.y >= this.enemyMaxY;

    // check we haven't passed min or max Y
    if (conditionUp || conditionDown) {
      mover.speed *= -1;
    }

    // check enemy overlap

    const enemyRect = enemy.getBounds();

    // restart the scene
    if (Phaser.Geom.Intersects.RectangleToRectangle(playerRect, enemyRect)) {
      // console.log('Game Over');
      return this.gameOver();
    }
  });
};

gameScene.gameOver = function gameOver() {
  // initiated game over sequence
  this.isTerminating = true;

  // shake camera
  this.cameras.main.shake(500);

  // listen for event completion
  this.cameras.main.on('camerashakecomplete', () => {
    // fade out
    this.cameras.main.fade(500);
  }, this);

  this.cameras.main.on('camerafadeoutcomplete', () => {
    // restart the scene
    this.scene.restart();
  }, this);
};

// set configuration of the game
const config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene,
};

// `create a new game, pass the config
// eslint-disable-next-line no-unused-vars
const game = new Phaser.Game(config);
