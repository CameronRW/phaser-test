const path = require('path');

module.exports = {
  entry: './src/game.js',
  devServer: {
    contentBase: './dist',
  },
  output: {
    filename: 'game.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
    ],
  },
  mode: 'development',
};
